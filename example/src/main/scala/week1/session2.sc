object Session {
  def factorial(n:Int):Long = {
    def factorialIter(num:Int,acc:Long):Long =
      if(num == 0) acc else factorialIter(num-1,acc*num)
    factorialIter(n,1)
  }
  factorial(3)
}