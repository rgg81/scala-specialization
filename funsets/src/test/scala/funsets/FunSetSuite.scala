package funsets

import org.scalatest.FunSuite


import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 *  - run the "test" command in the SBT console
 *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {

  /**
   * Link to the scaladoc - very clear and detailed tutorial of FunSuite
   *
   * http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite
   *
   * Operators
   *  - test
   *  - ignore
   *  - pending
   */

  /**
   * Tests are written using the "test" operator and the "assert" method.
   */
  // test("string take") {
  //   val message = "hello, world"
  //   assert(message.take(5) == "hello")
  // }

  /**
   * For ScalaTest tests, there exists a special equality operator "===" that
   * can be used inside "assert". If the assertion fails, the two values will
   * be printed in the error message. Otherwise, when using "==", the test
   * error message will only say "assertion failed", without showing the values.
   *
   * Try it out! Change the values so that the assertion fails, and look at the
   * error message.
   */
  // test("adding ints") {
  //   assert(1 + 2 === 3)
  // }


  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }

  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   *
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   *
   *   val s1 = singletonSet(1)
   *
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   *
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   *
   */

  trait TestSets {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
    val s4:Set = input => (input < 0) && (input > -6)
    val s5:Set = input => (input >= 0) && (input < 6)
    val s6:Set = input => (input >= 4) && (input < 10)
    val sAll:Set = input => (input >= -1000) && (input <= 1000)
  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   *
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSet(1) contains 1") {

    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3".
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "Singleton")
    }
  }

  test("union contains all elements of each set") {
    new TestSets {
      val s = union(s1, s2)
      assert(contains(s, 1), "Union 1")
      assert(contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
    }
  }

  test("intersection contains the intersection of the two given sets") {
    new TestSets {
      val s45 = intersect(s4, s5)
      val s56 = intersect(s5, s6)
      assert(!contains(s45, 1))
      assert(!contains(s45, -1))
      assert(!contains(s56, -1))
      assert(contains(s56, 5))
    }
  }

  test("diff of the two given sets") {
    new TestSets {
      val s56 = diff(s5, s6)
      assert(contains(s56, 0))
      assert(contains(s56, 1))
      assert(!contains(s56, 4))
      assert(!contains(s56, 5))
    }
  }

  test("filter sets") {
    new TestSets {
      val sFiltered = filter(s4, input => input == -3)
      assert(!contains(sFiltered, 0))
      assert(contains(sFiltered, -3))
    }
  }

  test("forall sets") {
    new TestSets {
      val result1 = forall(sAll,sAll)
      val result2 = forall(sAll,s4)

      val filter1:(Int => Boolean) = input => (input >= -10) && (input <= 10)
      val filter2:(Int => Boolean) = input => (input >= 0) && (input <= 10)

      val result3 = forall(s4,filter1)
      val result4 = forall(s4,filter2)
      assert(result1)
      assert(!result2)
      assert(result3)
      assert(!result4)
    }
  }

  test("exists sets") {
    new TestSets {
      val result1 = exists(sAll,sAll)
      val result2 = exists(sAll,s4)

      val filter1:(Int => Boolean) = input => (input >= -10) && (input <= 10)
      val filter2:(Int => Boolean) = input => (input >= 0) && (input <= 10)
      val filter3:(Int => Boolean) = input => (input >= -1) && (input <= 4)

      val result3 = exists(s4,filter1)
      val result4 = exists(s4,filter2)
      val result5 = exists(s4,filter3)
      assert(result1)
      assert(result2)
      assert(result3)
      assert(!result4)
      assert(result5)
    }
  }

  test("map sets") {
    new TestSets {

      val mapFunc:(Int => Int) = input => input * 2

      val result3 = map(s4,mapFunc)
      assert(contains(result3,-10))
      assert(contains(result3,-8))
      assert(contains(result3,-6))

    }
  }


}
