package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] =
    for {
      k <- arbitrary[Int]
      m <- oneOf(const(empty), genHeap)
    } yield insert(k,m)
  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }

  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("hint1: If you insert any two elements into an empty heap, finding the minimum of the resulting heap should get the smallest of the two elements back") = forAll { (element1:Int, element2:Int) =>
    val minimum = element1 min element2
    findMin(insert(element2,insert(element1,empty))) == minimum
  }

  property("hint2: If you insert an element into an empty heap, then delete the minimum, the resulting heap should be empty.") = forAll { (element1:A) =>
    val emptyHeap = deleteMin(insert(element1,empty))
    isEmpty(emptyHeap)
  }

  property("hint3: Given any heap, you should get a sorted sequence of elements when continually finding and deleting minima.") = forAll { (h:H) =>
    def findMinAndCheck(h:H,acc:A, result:Boolean): Boolean = {
      if (isEmpty(h) || !result) result
      else {
        val m = findMin(h)
        findMinAndCheck(deleteMin(h),m, acc <= m)
      }
    }
    findMinAndCheck(deleteMin(h),findMin(h),result = true)
  }

  property("hint4: Finding a minimum of the melding of any two heaps should return a minimum of one or the other.") = forAll { (h1:H, h2:H) =>
    val m1 = findMin(h1)
    val m2 = findMin(h2)
    val merged = meld(h1,h2)
    val minMerged = findMin(merged)
    minMerged == m1 || minMerged == m2
  }

  property("Heap Test Sort") =
    forAll(Gen.choose(0, 100)) { (n: Int) =>
      def toList(heap: H): List[Int] = {
        isEmpty(heap) match {
          case true => {
            Nil
          }
          case false => {
            val m = findMin(heap)
            val remaining = deleteMin(heap)
            m :: toList(remaining)
          }
        }
      }

      def toHeap(h: H, list: List[Int]): H = {
        list match {
          case Nil => h
          case t :: ts => toHeap(insert(t, h), ts)
        }
      }
      val toInsert = (1 to n).toList
      val retrievedElems = toList(toHeap(empty, toInsert))
      retrievedElems == toInsert
    }

}
