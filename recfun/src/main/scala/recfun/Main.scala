package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int =
    if(c <= 0) 1 else if (r <= 0) 0 else
       pascal(c-1, r-1) + pascal(c, r-1)

  
  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    def balanceIter(in:List[Char],balanceParam:Int):Boolean =
      (in,balanceParam) match {
        case (('(' :: tail),_) =>
          balanceIter(tail, balanceParam + 1)
        case ((')' :: tail),balance) if balance > 0 =>
          balanceIter(tail, balanceParam - 1)
        case ((')' :: tail),balance) if balance <= 0 =>
          false
        case ((head :: tail),balance) if balance >= 0 =>
          balanceIter(tail, balanceParam)
        case (_,balance) if balance < 0 =>
          false
        case (Nil,0) => true
        case (Nil,_) => false
        case _ => throw new RuntimeException("why you are here?")
      }
    balanceIter(chars,0)
  }
  
  /**
   * Exercise 3
   */
    def countChange(money: Int, coins: List[Int]): Int =
      if(money == 0) 1
      else if (money < 0)  0
      else if (coins.isEmpty && money > 0) 0
      else countChange(money, coins.tail) + countChange(money - coins.head, coins)
  }
