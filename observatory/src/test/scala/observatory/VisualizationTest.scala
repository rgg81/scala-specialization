package observatory


import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.scalatest.prop.Checkers

@RunWith(classOf[JUnitRunner])
class VisualizationTest extends FunSuite with Checkers {

  test("temperature interpolation 1") {
    val points:Iterable[(Double,Color)] = Iterable (
      (-60.0,Color(0,0,0)),
      (-50.0,Color(33,0,107)),
      (-27.0,Color(255,0,255)),
      (-15.0,Color(0,0,255)),
      (0.0,Color(0,255,255)),
      (12.0,Color(255,255,0)),
      (32.0,Color(255,0,0)),
      (60.0,Color(255,255,255))
    )
    assert(Visualization.interpolateColor(points,61.0) === Color(255,255,255))
    assert(Visualization.interpolateColor(points,45.0) === Color(255,118,118))
    assert(Visualization.interpolateColor(points,6.0) === Color(128,255,128))
    assert(Visualization.interpolateColor(points,-18.0) === Color(64,0,255))
    assert(Visualization.interpolateColor(points,-78.0) === Color(0,0,0))
  }

  test("temperature interpolation 2") {
    val points:Iterable[(Double,Color)] = Iterable (
      (-2.147483648E9,Color(255,0,0)),
      (-1.0,Color(0,0,255))
    )
    assert(Visualization.interpolateColor(points,-1.0737418245E9) === Color(128,0,128))

  }

  test("temperature interpolation 3") {
    val points:Iterable[(Double,Color)] = Iterable (
      (0.0,Color(255,0,0)),
      (11.0,Color(0,0,255))
    )
    assert(Visualization.interpolateColor(points,5.5) === Color(128,0,128))

  }

  test("temperature prediction") {
    val resultLocateTemp = Extraction.locateTemperatures(2015,"/stationsTest.csv","/tempTest.csv")
    val resultLocationYearly = Extraction.locationYearlyAverageRecords(resultLocateTemp)
    val predictedTemp = Visualization.predictTemperature(resultLocationYearly,Location(37.39, -78.439))
    assert(predictedTemp === 11.179090279474087)
  }

  test("visualize") {
    val tempLocations = List(
      (Location(45.0,-90.0),1.0),
      (Location(-45.0,0.0),18.718102044701453)
    )
    val colors = List(
      (1.0,Color(255,0,0)),
      (18.718102044701453,Color(0,0,255))
    )
    println(Visualization.visualize(tempLocations,colors).pixels(0).toColor)
  }

}
