package observatory

import java.time.LocalDate

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ExtractionTest extends FunSuite {

  test("locateTemperatures and locationYearly") {
    val resultLocateTemp = Extraction.locateTemperatures(2015,"/stationsTest.csv","/tempTest.csv")
    assert(resultLocateTemp === Seq(
      (LocalDate.of(2015, 8, 11), Location(37.35, -78.433), 27.299999999999997),
      (LocalDate.of(2015, 12, 6), Location(37.358, -78.438), 0.0),
      (LocalDate.of(2015, 1, 29), Location(37.358, -78.438), 2.000000000000001)
    ))

    val resultLocationYearly = Extraction.locationYearlyAverageRecords(resultLocateTemp)
    assert(resultLocationYearly === Map(
      (Location(37.35, -78.433), 27.299999999999997),
      (Location(37.358, -78.438), 1.0000000000000004)
    ))
  }
}