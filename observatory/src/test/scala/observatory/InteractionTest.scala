package observatory

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.scalatest.prop.Checkers

import scala.collection.concurrent.TrieMap

@RunWith(classOf[JUnitRunner])
class InteractionTest extends FunSuite with Checkers {

  test("tile to Image") {
    val tempLocations = List(
      (Location(45.0,-90.0),1.0),
      (Location(-45.0,0.0),18.718102044701453)
    )
    val colors = List(
      (1.0,Color(255,0,0)),
      (18.718102044701453,Color(0,0,255))
    )
    Interaction.tile(tempLocations,colors,1,1,1)
  }
}
