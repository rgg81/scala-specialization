package observatory

import java.time.LocalDate

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

/**
  * 1st milestone: data extraction
  */
object Extraction {

  /**
    * @param year             Year number
    * @param stationsFile     Path of the stations resource file to use (e.g. "/stations.csv")
    * @param temperaturesFile Path of the temperatures resource file to use (e.g. "/1975.csv")
    * @return A sequence containing triplets (date, location, temperature)
    */
  def locateTemperatures(year: Int, stationsFile: String, temperaturesFile: String): Iterable[(LocalDate, Location, Double)] = {
    def splitLine(line:String) = line.split(",",-1)
    val stationLines = scala.io.Source.fromInputStream(getClass.getResourceAsStream(stationsFile)).getLines
    val stations = stationLines.collect {
      case line if {
        val Array(_,_,lat,long) = splitLine(line)
        lat.nonEmpty && long.nonEmpty
      } =>
        val Array(stn,wban,lat,long) = splitLine(line)
        (s"$stn-$wban", Location(lat.toDouble, long.toDouble))
    }.toMap

    val temperatures = getClass.getResourceAsStream(temperaturesFile)
    val temperaturesLines = scala.io.Source.fromInputStream(temperatures).getLines
    temperaturesLines.collect {case line if {
      val Array(stn,wban,_,_,_) = splitLine(line)
      stations.get(s"$stn-$wban").isDefined
    } =>
      val Array(stn,wban,month,day,temp) = splitLine(line)
      (LocalDate.of(year, month.toInt, day.toInt),stations(s"$stn-$wban"),(temp.toDouble - 32) * 5.0/9.0)
    }.toIterable
  }

  /**
    * @param records A sequence containing triplets (date, location, temperature)
    * @return A sequence containing, for each location, the average temperature over the year.
    */
  def locationYearlyAverageRecords(records: Iterable[(LocalDate, Location, Double)]): Iterable[(Location, Double)] = {
    records.groupBy{ item =>
      item._2
    }.mapValues(values => values.map(_._3).sum/values.size)
  }

}
