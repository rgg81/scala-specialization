package observatory


import com.sksamuel.scrimage.{Image, Pixel, RGBColor}

import scala.collection.parallel.ForkJoinTaskSupport
import scala.concurrent.forkjoin.ForkJoinPool

/**
  * 2nd milestone: basic visualization
  */
object Visualization {

  /**
    * @param temperatures Known temperatures: pairs containing a location and the temperature at this location
    * @param location Location where to predict the temperature
    * @return The predicted temperature at `location`
    */
  def predictTemperature(temperatures: Iterable[(Location, Double)], location: Location): Double = {
    val locationFound = temperatures.find(_._1 == location)
    locationFound match {
      case Some(loc) => loc._2
      case None =>idw(temperatures, location)
    }
  }

  private def distance(loc1:Location,loc2:Location) = {
    val R = 6371e3
    val dLat = (loc1.lat - loc2.lat).toRadians
    val dLon = (loc1.lon - loc2.lon).toRadians

    import scala.math._
    val a = sin(dLat / 2) * sin(dLat / 2) + cos(loc1.lat.toRadians) * cos(loc2.lat.toRadians) * sin(dLon / 2) * sin(dLon / 2)
    val c = 2 * atan2(sqrt(a), sqrt(1 - a))
    R * c
  }

  private def idw(temperatures: Iterable[(Location, Double)], location: Location): Double = {

    val p = 2
    import scala.math._
    def simpleIdw(loc1:Location,loc2:Location):Double = {
      1 / pow(distance(loc1,loc2),p)
    }
    val result = temperatures.map{ case (loc,temp) =>
      val simpleIdwValue = simpleIdw(loc,location)
      (temp * simpleIdwValue, simpleIdwValue )
    }.reduce[(Double,Double)]{ case (item1,item2) =>
      (item1._1 + item2._1,item1._2 + item2._2)
    }
    result._1/result._2
  }

  /**
    * @param points Pairs containing a value and its associated color
    * @param value  The value to interpolate
    * @return The color that corresponds to `value`, according to the color scale defined by `points`
    */
  def interpolateColor(points: Iterable[(Double, Color)], value: Double): Color = {
    val ps = points.toSeq
    ps.indexWhere(_._1 >= value) match {
      case -1 => interpolate(points.init.last, points.last, value)
      case  0 => interpolate(points.head, points.tail.head, value)
      case  x => interpolate(ps(x - 1), ps(x), value)
    }
  }

  private def interpolate(p1: (Double, Color), p2: (Double, Color), value: Double): Color = {
    Color(
      interpolate(p1._1, p1._2.red,   p2._1, p2._2.red,   value),
      interpolate(p1._1, p1._2.green, p2._1, p2._2.green, value),
      interpolate(p1._1, p1._2.blue,  p2._1, p2._2.blue,  value)
    )
  }

  private def interpolate(temp1: Double, colorValue1: Int, temp2: Double, colorValue2: Int, value: Double): Int = {
    def round(value: Double):Int =
      math.max(0, math.min(255, math.round(value).toInt))
    round(colorValue1 + (value - temp1) * (colorValue2 - colorValue1) / (temp2 - temp1))
  }

  /**
    * @param temperatures Known temperatures
    * @param colors Color scale
    * @return A 360×180 image where each pixel shows the predicted temperature at its location
    */
  def visualize(temperatures: Iterable[(Location, Double)], colors: Iterable[(Double, Color)]): Image = {
    def fromPixelIndex(index: Int): Location = {
      def x: Int = index % 360
      def y: Int = index / 360
      Location(90 - y, x - 180)
    }

    val idxPar = (0 until (360 * 180)).par
    idxPar.tasksupport = new ForkJoinTaskSupport(new ForkJoinPool(4))
    val pixels = idxPar
      .map{ idx =>
        val loc = fromPixelIndex(idx)
        val temp = predictTemperature(temperatures, loc)
        val color = interpolateColor(colors, temp)
        Pixel(RGBColor(color.red, color.green, color.blue))
      }.toArray
    Image(360, 180, pixels)
  }

}

