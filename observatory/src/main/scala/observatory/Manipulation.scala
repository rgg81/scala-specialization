package observatory

import scala.collection.parallel.ParMap

/**
  * 4th milestone: value-added information
  */
object Manipulation {

  private val locations = for (lat <- -89 to 90; lon <- -180 to 179) yield Location(lat, lon)
  /**
    * @param temperatures Known temperatures
    * @return A function that, given a latitude in [-89, 90] and a longitude in [-180, 179],
    *         returns the predicted temperature at this location
    */
  def makeGrid(temperatures: Iterable[(Location, Double)]): (Int, Int) => Double = {
    val grid: ParMap[Location, Double] = locations.par
      .map(loc => loc -> Visualization.predictTemperature(temperatures, loc))
      .toMap
    (latitude:Int, longitude:Int) => grid(Location(latitude, longitude))
  }

  /**
    * @param temperaturess Sequence of known temperatures over the years (each element of the collection
    *                      is a collection of pairs of location and temperature)
    * @return A function that, given a latitude and a longitude, returns the average temperature at this location
    */
  def average(temperaturess: Iterable[Iterable[(Location, Double)]]): (Int, Int) => Double = {
    val allFuncs = temperaturess.map(makeGrid).par
    val averages:ParMap[Location, Double] = locations.par
        .map { location =>
          location -> {
              val sum = allFuncs.map(_(location.lat.toInt,location.lon.toInt)).sum
              sum/allFuncs.size
          }

        }.toMap
    (latitude:Int, longitude:Int) =>
      averages(Location(latitude,longitude))
  }

  /**
    * @param temperatures Known temperatures
    * @param normals A grid containing the “normal” temperatures
    * @return A grid containing the deviations compared to the normal temperatures
    */
  def deviation(temperatures: Iterable[(Location, Double)], normals: (Int, Int) => Double): (Int, Int) => Double = {
    val grid = makeGrid(temperatures)
    (latitude:Int, longitude:Int) =>
      val value = grid(latitude,longitude)
      val normal = normals(latitude,longitude)
      value - normal
  }


}

